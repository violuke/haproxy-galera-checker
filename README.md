# HaProxy Galera Checker

Script to make a proxy (ie HAProxy) capable of monitoring Galera Cluster nodes properly. Based upon https://github.com/obissick/galera-check

## Install instructions
```bash
# Get the code
cd /usr/bin/
sudo git clone https://gitlab.com/violuke/haproxy-galera-checker.git


# Configure
cd haproxy-galera-checker
sudo cp database.example.ini database.ini
sudo nano database.ini

# Install requirements
sudo apt install xinetd python3-pip
sudo -H pip3 install mysql.connector ConfigParser
```

Ensure you have a database user with acceptable permissions, by using something like
```sql
CREATE USER 'haproxy'@'localhost' IDENTIFIED BY 'super-password-here';
GRANT PROCESS ON *.* TO 'haproxy'@'localhost' WITH GRANT OPTION;
```

Configure xinetd to allow the check on port 9200
```bash
sudo nano /etc/xinetd.d/haproxy-galera-checker
```

```
# default: on
# description: haproxy-galera-checker
service haproxy-galera-checker
{
    disable = no
    flags = REUSE
    socket_type = stream
    port = 9200
    wait = no
    user = nobody
    server = /usr/bin/python3
    server_args = /usr/bin/haproxy-galera-checker/check.py
    log_on_failure += USERID
    per_source = UNLIMITED
}
```

Configure xinetd
```bash
# Edit file
sudo nano /etc/services

# Add this line somewhere (the bottom could be sensible)
haproxy-galera-checker    9200/tcp

sudo service xinetd restart
```

Validate you get a 200 response with
```bash
telnet localhost 9200
```

## Taking a node out for maintenance
Edit the config file

```bash
nano /usr/bin/haproxy-galera-checker/database.ini
```

Swap

```
down_for_maintenance=0
```

to 

```
down_for_maintenance=1
```
